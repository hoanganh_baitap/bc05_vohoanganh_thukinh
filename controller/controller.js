

// render ra giao dien
export const render = (glassArray) => {
    const glassList = document.getElementById('vglassesList');
    glassArray.forEach((glass) => {
        let newDiv = document.createElement('div');
        let newImg = document.createElement('img');
        newImg.src = glass.src;
        newImg.alt = glass.name;
        newImg.classList.add("w-100");
        newDiv.append(newImg);
        newDiv.classList.add("col-4");
        newDiv.addEventListener('click', () => {
            addGlassToFace(glass);
        })
        glassList.append(newDiv);
    })
}

// gắn kính lên mặt
const addGlassToFace =(glass)=>{
    progressGlass(glass);
    infoGlass(glass);
}

// xử lý kính
const progressGlass = (glass) => {
    let khuonMat = document.getElementById("avatar");
    removeGlass(khuonMat);
    addGlass(glass, khuonMat);
}
// gắn kính
const addGlass = (glass, khuonMat) => {
    let newDiv = document.createElement('div');
    let newImg = document.createElement('img');
    newImg.src = glass.virtualImg;
    newImg.alt = glass.name;
    newDiv.append(newImg);
    khuonMat.append(newDiv);
}

// xử lý thông tin kính
const infoGlass = (glass) => {
    let glassesInfo = document.getElementById('glassesInfo');
    let header = headerInfo(glass);
    let main = bodyInfo(glass);
    let footer = footerInfo(glass);
    removeGlass(glassesInfo);
    glassesInfo.append(header);
    glassesInfo.append(main);
    glassesInfo.append(footer);
    glassesInfo.classList.add('d-block');

}
// tháo kính
const removeGlass = (remove) =>{
    let child = remove.lastElementChild;
    while (child) {
        remove.removeChild(child);
        child = remove.lastElementChild;
    }
}

// header
const headerInfo = (glass) => {
    let header = document.createElement('div');
    let name = document.createElement('span');
    name.textContent = glass.name.toUpperCase() + " - ";
    let brand = document.createElement('span');
    brand.textContent = glass.brand + " ";
    let color = document.createElement('span');
    color.textContent = `(${glass.color})`;
    header.append(name);
    header.append(brand);
    header.append(color);
    header.classList.add("font-weight-bold");
    return header;
}
// body
const bodyInfo = (glass) => {
    let mainInfo = document.createElement('div');
    let price = document.createElement('span');
    price.classList.add('bg-danger');
    price.classList.add('px-2');
    price.classList.add('py-1');
    price.classList.add('d-inline-block');
    price.classList.add('rounded');

    price.textContent = `$${glass.price}`;
    let something = document.createElement('span');
    something.classList.add('text-success');
    something.textContent = " stocking";
    mainInfo.append(price);
    mainInfo.append(something);
    return mainInfo;
}
// footer
const footerInfo = (glass) => {
    let footerInfo = document.createElement('div');
    let des = document.createElement('span');
    des.textContent = glass.description;
    footerInfo.append(des);
    return footerInfo;
}
